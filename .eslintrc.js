// https://eslint.org/docs/user-guide/configuring

module.exports = {
  env: {
    browser: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    // but leaving it as "essential" for now until we get back the Feb 28 release
    'plugin:vue/essential',
    "plugin:css-modules/recommended"
  ],

  plugins: [
    // required to lint *.vue files
    'vue',
    "css-modules"
  ]
}
